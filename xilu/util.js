let core = require('./core.js');
const formatDatetime = function(date, fmt) { //author: meizz
  if(!date) {
    return "";
  }
  if(!(date instanceof Date)) {
    if(/^\d+$/.test(date)) {
      date = (date+"").length>10 ? date : date*1000;
      date = new Date(parseInt(date));
    }else {
      date = new Date(date.replace(/-/g,'/'));
    }
  }
  var o = {
    "m+": date.getMonth() + 1, //月份
    "d+": date.getDate(), //日
    "h+": date.getHours(), //小时
    "i+": date.getMinutes(), //分
    "s+": date.getSeconds(), //秒
    "q+": Math.floor((date.getMonth() + 3) / 3), //季度
    "S": date.getMilliseconds() //毫秒
  };
  var yearMatch = fmt.match(/(y+)/);
  if (yearMatch) {
    fmt = fmt.replace(yearMatch[0], (date.getFullYear() + "").substr(4 - yearMatch[0].length));
  }
  for (var k in o) {
    var match = fmt.match(new RegExp("(" + k + ")"));
    if (match)
      fmt = fmt.replace(match[0], (match[0].length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  }
  return fmt;
};
/**
 * 加载更多按钮功能
 * page.data定义moreButton(可由moreButtonDataField更改) moreButton.text作为按钮文本, moreButton.page作为页码
 * @param pageInstance
 * @param url 接口地址
 * @param queryData querystring parameters
 * @param moreButtonDataField morebutton data在page.data中的字段名
 * @param listFieldName 渲染的列表数据再page.data中的字段名
 * @param retDataFieldName 接口返回的列表数据再http response ret.data中的字段名
 * @param dataFormatter function, 处理服务器返回的列表数据，返回处理过后的数组, this指向pageInstance
 */
const fetch = function(pageInstance, url,queryData, moreButtonDataField, listFieldName, retDataFieldName, dataFormatter) {
  let moreButtonData = pageInstance[moreButtonDataField] || {page:1};
  if(moreButtonData.loading || moreButtonData.nomore) {
    return;
 }
  queryData.page = moreButtonData.page;
  pageInstance[moreButtonDataField]['loading'] = true;
  pageInstance[moreButtonDataField]['text'] = '正在加载...';
  pageInstance.$forceUpdate();
  core.get({
    url: url, data: queryData,
	loading:false,
    success(ret, response) {
      let listData = retDataFieldName?ret.data[retDataFieldName]:ret.data;
      let listLength = listData.length;
      if(typeof dataFormatter === 'function') {
        //listData = dataFormatter.apply(pageInstance, [listData, ret.data]) || listData;
		dataFormatter(ret.data);
      }else if(dataFormatter==='unique') {
        function uniqueFormatter(listData) {
          let formattedList = [];
          out:  for(let i in listData) {
            let item = listData[i];
            for(let j in pageInstance[listFieldName]) {
              let originItem = pageInstance[listFieldName][j];
              if(originItem.id==item.id) {
                continue out;
              }
            }
            formattedList.push(item);
          }
          return formattedList;
        }
        listData = uniqueFormatter(listData);
      }
      if(Array.isArray(listData) && listLength>0) {
        pageInstance[listFieldName] = (pageInstance[listFieldName] || []).concat(listData);
        let pagesize = queryData.pagesize || 10;
        
        if(listLength<pagesize) {
		  pageInstance[moreButtonDataField]['page'] = moreButtonData.page + 1;
		  pageInstance[moreButtonDataField]['loading'] = false;
		  pageInstance[moreButtonDataField]['nomore'] = true;
		  pageInstance[moreButtonDataField]['text'] = '—— 我是有底线的 ——';
          //console.log(pageInstance,1)
        }else {
			
			pageInstance[moreButtonDataField]['page'] = moreButtonData.page + 1;
			pageInstance[moreButtonDataField]['loading'] = false;
			pageInstance[moreButtonDataField]['text'] = '加载更多';
			//console.log(pageInstance,2)
        }
      }else {
        let text = moreButtonData.page == 1 ? '暂无数据' : '—— 我是有底线的 ——';
        let nothing = moreButtonData.page == 1 ? true : false;
		pageInstance[moreButtonDataField]['nomore'] = true;
		pageInstance[moreButtonDataField]['text'] = text;
		pageInstance[moreButtonDataField]['nothing'] = nothing;
		//console.log(pageInstance,3)
      }
	  pageInstance.$forceUpdate();
    },
    fail(ret, response) {
		pageInstance[moreButtonDataField]['loading'] = false;
		pageInstance[moreButtonDataField]['text'] = '加载更多';
    }
  })
};
/**
 * 加载更多按钮功能
 * page.data定义moreButton(可由moreButtonDataField更改) moreButton.text作为按钮文本, moreButton.page作为页码
 * @param pageInstance
 * @param url 接口地址
 * @param queryData querystring parameters
 * @param moreButtonDataField morebutton data在page.data中的字段名
 * @param listFieldName 渲染的列表数据再page.data.list中的字段名
 * @param retDataFieldName 接口返回的列表数据再http response ret.data中的字段名
 * @param dataFormatter function, 处理服务器返回的列表数据，返回处理过后的数组, this指向pageInstance
 */
const fetch2 = function(pageInstance, url,queryData, moreButtonDataField, listFieldName,firstDataField,firstIndex, retDataFieldName, dataFormatter) {
	//console.log(pageInstance[firstDataField][firstIndex],firstDataField)
  let moreButtonData = pageInstance[firstDataField][firstIndex][moreButtonDataField] || {page:1};
  console.log(moreButtonData)
  if(moreButtonData.loading || moreButtonData.nomore) {
    return;
 }
  queryData.page = moreButtonData.page;
  pageInstance[firstDataField][firstIndex][moreButtonDataField]['loading'] = true;
  pageInstance[firstDataField][firstIndex][moreButtonDataField]['text'] = '正在加载...';
  pageInstance.$forceUpdate();
  core.get({
    url: url, data: queryData,
	loading:false,
    success(ret, response) {
      let listData = retDataFieldName?ret.data[retDataFieldName]:ret.data;
      let listLength = listData.length;
      if(typeof dataFormatter === 'function') {
        //listData = dataFormatter.apply(pageInstance, [listData, ret.data]) || listData;
		dataFormatter(ret.data);
      }else if(dataFormatter==='unique') {
        function uniqueFormatter(listData) {
          let formattedList = [];
          out:  for(let i in listData) {
            let item = listData[i];
            for(let j in pageInstance[firstDataField][firstIndex][listFieldName]) {
              let originItem = pageInstance[firstDataField][firstIndex][listFieldName][j];
              if(originItem.id==item.id) {
                continue out;
              }
            }
            formattedList.push(item);
          }
          return formattedList;
        }
        listData = uniqueFormatter(listData);
      }
      if(Array.isArray(listData) && listLength>0) {
        pageInstance[firstDataField][firstIndex][listFieldName] = (pageInstance[firstDataField][firstIndex][listFieldName] || []).concat(listData);
        let pagesize = queryData.pagesize || 10;
        
        if(listLength<pagesize) {
		  pageInstance[firstDataField][firstIndex][moreButtonDataField]['page'] = moreButtonData.page + 1;
		  pageInstance[firstDataField][firstIndex][moreButtonDataField]['loading'] = false;
		  pageInstance[firstDataField][firstIndex][moreButtonDataField]['nomore'] = true;
		  pageInstance[firstDataField][firstIndex][moreButtonDataField]['text'] = '—— 我是有底线的 ——';
          //console.log(pageInstance,1)
        }else {
			
			pageInstance[firstDataField][firstIndex][moreButtonDataField]['page'] = moreButtonData.page + 1;
			pageInstance[firstDataField][firstIndex][moreButtonDataField]['loading'] = false;
			pageInstance[firstDataField][firstIndex][moreButtonDataField]['text'] = '加载更多';
			//console.log(pageInstance,2)
        }
      }else {
        let text = moreButtonData.page == 1 ? '暂无数据' : '—— 我是有底线的 ——';
        let nothing = moreButtonData.page == 1 ? true : false;
		pageInstance[firstDataField][firstIndex][moreButtonDataField]['nomore'] = true;
		pageInstance[firstDataField][firstIndex][moreButtonDataField]['text'] = text;
		pageInstance[firstDataField][firstIndex][moreButtonDataField]['nothing'] = nothing;
		//console.log(pageInstance,3)
      }
	  pageInstance.$forceUpdate();
    },
    fail(ret, response) {
		pageInstance[firstDataField][firstIndex][moreButtonDataField]['loading'] = false;
		pageInstance[firstDataField][firstIndex][moreButtonDataField]['text'] = '加载更多';
    }
  })
};
const getRemoteDataCached = async function(options) {
  let data, url = options.url, cacheKey = options.cacheKey, useCache = options.useCache===undefined ? true : options.useCache,
    retDataKey = options.dataKey;
  if(useCache) {
    data = core.getCache(cacheKey);
  }
  if(!data) {
    data = await (new Promise(function(resolve, reject) {
      core.get({
        url: url,
        data: options.data || '',
        loading:false,
        success(ret) {
          resolve(retDataKey ? ret.data[retDataKey]:ret.data);
        },
        fail(ret) {
          reject(ret);
        }
      });
    }));
    core.setCache(cacheKey, data, 1800);
  }
  return data;
};

/**
 * 获取城市数据，包含省份
 * @param {boolean} useCache true则先从缓存中获取，否则直接取网络并放入缓存
 */
const getAreaTree = function (useCache=true) {
  let cacheKey = 'area_tree';
  return getRemoteDataCached({
    url: '/xilumarket.common/area_tree',
    data: {},
    cacheKey: cacheKey,
    useCache: useCache,
    dataKey: ''
  });
};

module.exports = {
	formatDatetime:formatDatetime,
	fetch:fetch,
	fetch2:fetch2,
	getAreaTree:getAreaTree,
};
